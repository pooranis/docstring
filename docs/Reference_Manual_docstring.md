Package 'docstring'
================
February 17, 2020


```
Type: Package
Package: docstring
Title: Provides Docstring Capabilities to R Functions
Version: 1.0.0
Date: 2017-03-16
Authors@R (parsed):
    * Dason Kurkiewicz <dasonk@gmail.com> [aut, cre]
    * Neal Fultz <nfultz@gmail.com> [ctb]
Author: Dason Kurkiewicz [aut, cre], Neal Fultz [ctb]
Maintainer: Dason Kurkiewicz <dasonk@gmail.com>
Description: Provides the ability to display something analogous to
    Python's docstrings within R.  By allowing the user to document their
    functions as comments at the beginning of their function without
    requiring putting the function into a package we allow more users to
    easily provide documentation for their functions.  The documentation
    can be viewed just like any other help files for functions provided by
    packages as well.
License: GPL-2
URL: https://github.com/dasonk/docstring
BugReports: https://github.com/dasonk/docstring/issues?state=open
Imports:
    roxygen2,
    utils,
    tools
Suggests:
    devtools,
    rstudioapi,
    knitr,
    rmarkdown
VignetteBuilder:
    knitr
Roxygen: list(old_usage = TRUE)
RoxygenNote: 7.0.2
```


#  R topics documented:
  - [`docstring`](#docstring)
  - [`fixhelppanelink`](#fixhelppanelink)

<!-- toc -->

<small>[top](#r-topics-documented)</small>

# `docstring`

Display a docstring

## Description

Display a docstring using R's built in help file viewer.

## Usage

``` r
docstring(fun, fun_name = as.character(substitute(fun)),
rstudio_pane = getOption("docstring_rstudio_help_pane"),
default_title = "Title not detected", old_usage=T, verbose=F)
```

## Arguments

| Argument        | Description                                                                                                                                                                                                                               |
| --------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `fun`           | The function that has the docstring you would like to display                                                                                                                                                                             |
| `fun_name`      | The name of the function.                                                                                                                                                                                                                 |
| `rstudio_pane`  | logical. If running in RStudio do you want the help to show in the help pane? This defaults to TRUE but can be explicitly set using options("docstring\_rstudio\_help\_pane" = TRUE) or options("docstring\_rstudio\_help\_pane" = FALSE) |
| `default_title` | The title you would like to display if no title is detected in the docstring itself.                                                                                                                                                      |
| `old_usage`     | Use old roxygen2 wrapped usage                                                                                                                                                                                                            |
| `verbose`       | debug messages                                                                                                                                                                                                                            |

## Examples

``` r
## Not run:
 square <- function(x){
#' Square a number
#'
#' Calculates the square of the input
#'
#' @param x the input to be squared

return(x^2)
}

docstring(square)
?square


mypaste <- function(x, y = "!"){
#' Paste two items
#'
#' @description This function pastes two items
#' together.
#'
#' By using the description tag you'll notice that I
#' can have multiple paragraphs in the description section
#'
#' @param x character. The first item to paste
#' @param y character. The second item to paste Defaults to "!" but
#' "?" would be pretty great too
#' @usage mypaste(x, y)
#' @return The inputs pasted together as a character string.
#' @details The inputs can be anything that can be input into
#' the paste function.
#' @note And here is a note. Isn't it nice?
#' @section I Must Warn You:
#' The reference provided is a good read.
#' \subsection{Other warning}{
#'   It is completely irrelevant to this function though.
#' }
#'
#' @references Tufte, E. R. (2001). The visual display of
#' quantitative information. Cheshire, Conn: Graphics Press.
#' @examples
#' mypaste(1, 3)
#' mypaste("hey", "you")
#' mypaste("single param")
#' @export
#' @importFrom base paste

return(paste(x, y))
}

?mypaste 
## End(Not run)
```

<small>[top](#r-topics-documented)</small>

# `fixhelppanelink`

Fix links for help panel

## Description

Fix links in Rd file for use with RStudio help panel

## Usage

``` r
fixhelppanelink(Rdfile, pkgname = NULL, out = NULL)
```

## Arguments

| Argument  | Description                                     |
| --------- | ----------------------------------------------- |
| `Rdfile`  | R documentation file                            |
| `pkgname` | Name of package where Rd file is from (or NULL) |
| `out`     | output Rd file, if NULL not written             |

## Value

lines from edited Rd file
